# corentinM2

Analysis of GNS and GTS métiers VMS data to assess fishing strategy and any changes therein over time.
Host all codes developped by Corentin during his Master 2 thesis at Observatoire Pelagis

Collaborators
 - Authier Matthieu (Adera)
 - Deslias Camille (LRUniv)
 - Vignard Corentin (AgroCampus Rennes)
 - Peltier Hélène (Adera)
