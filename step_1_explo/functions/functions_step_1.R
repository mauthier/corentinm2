################################
df_to_sf <- function(df) {
  if(any(names(df) == "LONGITUDE") && any(names(df) == "LATITUDE")) {
                                 out <- df %>% 
                                   as.data.frame() %>% 
                                   st_as_sf(coords = c("LONGITUDE","LATITUDE"),
                                            crs = 4326
                                   )
                               } else {
                                 out <- NULL
                               }
                               return(out)
                             }
################################

################################
ping_in_buffer_port <- function(df, buffer, proj_epsg = 4326) {
  # df est un dataframe avec les pings et une colonne LONGITUDE et LATITUDE en WGS84
  # buffer est un objet sf en WGS84 avec les ports et un buffer autour
  ### verifier que la projection est bonne
  if(st_crs(buffer)$input != "WGS 84") {
    stop("Please check projection of buffer: must be WGS84")
  }
  ### rajouter une indicatrice dans un port (objet buffer)
  df <- df %>% 
    mutate(POSITION = "FALSE")
  ### verifier que l'on a des coordonnées
    out <- df %>% 
      st_transform(crs = proj_epsg) %>% 
      ### Pings : inside ou outside du buffer? TRUE ou FALSE
      st_within(buffer %>% 
                  st_transform(crs = proj_epsg), 
                sparse = FALSE
               ) %>% 
      as.data.frame() 
    ### Recupere le numero des lignes des pings inside du buffer (= TRUE)
    if(sum(apply(out, 1, sum)) != 0) { # si somme =/= 0 : il y a des TRUE (sinon que des FALSE)
      out <- which(out == "TRUE", arr.ind = TRUE) %>%
        as.data.frame() %>% 
        select(row)
      ### Associe la position (TRUE or FALSE) des pings par rapport au buffer (ligne out => TRUE)
      df$POSITION[drop(out$row)] <- "TRUE"
    }
  ### Renvoi un df avec une nouvelle colonne indiquant si le ping tombe dans le buffer
  return(df)
}
################################

################################
tide <- function(df) {
  df <- df %>% mutate(FT_REF = 1)
  for (i in 1:length(df$FT_REF)){
if (df$POSITION[i] == "TRUE" && df$INTV[i] > 60){
  df$FT_REF[i] <- max(df$FT_REF) + 1 
}else{
  df$FT_REF[i] <- max(df$FT_REF)
  }
  }
  return(df)
}
################################

################################
tide_autre_methode <- function(df) {
  df <- df %>% mutate(NEW_FT_REF = 1)
  for (i in 1:length(df$NEW_FT_REF)){
    if (df$INTV[i] > 500|(df$POSITION[i] == "TRUE" && df$INTV[i] > 50)){
      df$NEW_FT_REF[i] <- max(df$NEW_FT_REF) + 1 
    }else{
      df$NEW_FT_REF[i] <- max(df$NEW_FT_REF)
    }
  }
  return(df)
}
################################

################################
long_tide <- function(df) {
c <- which(table(df$NEW_FT_REF) > 20, arr.ind = TRUE) %>% as.data.frame() %>% row.names()
df <- df[df$NEW_FT_REF %in% c,]
return(df)
}
################################

################################
interpolation <- function(df) {
  if (nrow(df) != 0) {
    for (i in 1:length(df$VE_REF)-1){
      df$geometry[i] <- st_combine(c(df$geometry[i],df$geometry[i+1]))
      df$geometry[i] <- st_cast(df$geometry[i], "LINESTRING")
    }
  }
  return(df)
}
################################

################################
buffer_AIS <- function(df_AIS) {
  df_AIS %>% 
    st_transform(crs = 3035) %>% 
    st_buffer(., dist = 0.02, crs = 4326)
    return(df_AIS)
}
################################

################################
ping_in_buffer_AIS <- function(df_VMS, buffer_AIS) {
  out <- out %>% lapply(df_VMS,st_within(.,buffer_AIS,sparse=FALSE)) %>% as.data.frame()
  return(out)
}
################################

################################
is_fishing <- function(df) {
  df <- df %>% mutate(IS_FISHING = "FALSE")
  for (i in 1:length(df$VITESSE_CALCULEE))
  {
    if (df$VITESSE_CALCULEE[i] < 4.5)
    {df$IS_FISHING[i] <- "TRUE"}
  }
  return(df)
}
################################

################################
linestring <- function(df) {
  df$geometry <- df %>% st_transform(crs = 2154) %>% 
    st_union() %>% st_cast(to = "LINESTRING") %>% 
    st_transform(crs = 4326)
  return(df)
}
################################