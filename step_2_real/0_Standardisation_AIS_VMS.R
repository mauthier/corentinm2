##--------------------------------------------------------------------------------------------------------
## SCRIPT : Formatage, nettoyage et ecriture des jeux de donnees AIS et VMS pour identifier les navires
##
## Authors : Corentin Vignard 
## Last update : 2021-12-08
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

rm(list=ls(all=TRUE))
lapply(c("dplyr","tidyverse","doBy","sqldf","purrr","lubridate","readxl"), library, character.only = TRUE)

### Changement du nom du fichier en fonction de l'annee et du mois souhaite
Mois <- 1
Annee <- 2017
file_name_AIS = paste("./data/step_2_real/",Annee,"_AIS/Month/",Mois,".csv",sep="")
file_name_VMS = paste("./data/step_2_real/",Annee,"_VMS/","VMS_VIII_",Annee,".csv",sep="")
###

######## Chargement des donnees ######## 
AIS = read.csv(file = file_name_AIS, quote = "", row.names = NULL, sep = ";", 
                          dec = ".", fileEncoding = "latin1", stringsAsFactors = FALSE)
VMS = read.csv(file = file_name_VMS, quote = "", row.names = NULL, sep = ";",
                          dec = ".", fileEncoding = "latin1", stringsAsFactors = FALSE)
rm(file_name_AIS,file_name_VMS)
################ 

######## Formatage et nettoyage du jeu de donnees AIS ######## 
### Suppression des colonnes inutiles et renommage
nbre_pings_bruts <- nrow(AIS)
AIS = AIS[,-(c(3,4,5,10,16,24,26:29))]
colnames(AIS) <- c("MMSI","Version_AIS","Type_navire","Longueur","Largeur","EPFD","Classe",
                              "Horodatage","Statut_navigation","Taux_giration_min","Vitesse","Longitude",
                              "Latitude","Cap","Cap_vrai","Seconde_UTC","Manoeuvre","RAIM","Vitesse_calculee") %>% toupper()
### Suppression des lignes doublons 
AIS <- unique(AIS)
### Conversion de l'horodatage 
AIS$HORODATAGE <- as_datetime(AIS$HORODATAGE)
### Renommage du MMMSI => VE_REF (necessaire pour ordonner vmstools::sortTacsat()) et de l'horodatage
AIS <- AIS %>% rename(VE_REF = MMSI) %>% rename(DATE = HORODATAGE)
AIS$VE_REF <- as.factor(AIS$VE_REF)
### Ajout de SI_DATE et SI_TIME (necessaire pour ordonner vmstools::sortTacsat())
AIS$SI_DATE <- substr(AIS$DATE, 1,10) %>% as.Date() %>% format("%d/%m/%Y") # Ne recupere que la date
AIS$SI_TIME <- substr(AIS$DATE, 12,19) # Ne recupere que l'heure
### Restriction aux pings de l'annee X et au mois Y (-1 et + 1)
AIS <- AIS %>% filter((year(DATE) == Annee & month(DATE) == Mois-1) |
                      (year(DATE) == Annee & month(DATE) == Mois) |
                      (year(DATE) == Annee & month(DATE) == Mois+1))  
### Restriction aux pings situes dans le golfe de Gascogne (Zone CIEM VIII a,b,c,d)
AIS <- AIS %>% filter(LATITUDE >= 43 & LATITUDE <= 48 & LONGITUDE >= -11 & LONGITUDE <= -0.3)
# www.fao.org/fishery/area/Area27/en#FAO-fishing-area-27.8
### Restriction aux navires de peche
AIS <- AIS %>% filter(TYPE_NAVIRE == "30")
### Suppression des pings avec un MMSI inchoerent 
wrongMMSI= c(000000000,111111111,222222222,333333333,444444444,555555555,
             666666666,777777777,888888888,999999999,123456789,0,12345) %>% as.numeric()
AIS <- AIS %>% filter(VE_REF != "wrongMMSI")
rm(clear = wrongMMSI)
### Suppression des pings avec une vitesse aberrante
AIS <- AIS %>% filter(VITESSE <= 20)
### Tri des pings par bateau puis par date
AIS <- sortTacsat(AIS)
### Mise en ordre des colonnes
AIS <- AIS[, c("VE_REF","TYPE_NAVIRE","LONGUEUR","LARGEUR","RAIM",
                                     "EPFD","VERSION_AIS","CLASSE","SI_DATE","SI_TIME",
                                     "SECONDE_UTC","LONGITUDE","LATITUDE","CAP","CAP_VRAI","TAUX_GIRATION_MIN",
                                     "STATUT_NAVIGATION","VITESSE","VITESSE_CALCULEE","MANOEUVRE","SI_DATIM")]
nbre_pings_post_traitement <- nrow(AIS)
################ 

######## Formatage et nettoyage du jeu de donnees VMS ######## 
### Suppression des lignes doublons 
VMS <- unique(VMS)
### Conversion de l'horodatage
VMS$DATE <- as.POSIXct(VMS$DATE, format = "%d/%m/%Y %H:%M") %>% with_tz(tzone = "UTC")
### Ajout de SI_DATE et SI_TIME (necessaire pour ordonner vmstools::sortTacsat())
VMS$SI_DATE <- substr(VMS$DATE, 1,10) %>% as.Date() %>% format("%d/%m/%Y")
VMS$SI_TIME <- substr(VMS$DATE, 12,19) 
### Renommage des colonnes
VMS <- VMS %>% rename(VITESSE = AVG_SPEED)
### Restriction aux pings de l'annee X et au mois Y
VMS <- VMS %>% filter((year(DATE) == Annee & month(DATE) == "1")) 
                     # |(year(DATE) == Annee & month(DATE) == "2")
                     # |(year(DATE) == Annee & month(DATE) == "3"))
### Restriction aux pings situes dans le golfe de Gascogne (Zone CIEM VIII a,b,c,d)
VMS <- VMS %>% filter(LATITUDE >= 43 & LATITUDE <= 48 & LONGITUDE >= -11 & LONGITUDE <= -0.3)
# www.fao.org/fishery/area/Area27/en#FAO-fishing-area-27.8
### Suppression des pings avec une vitesse aberrante
VMS <- VMS %>% filter(VITESSE <= 20)
### Trie des pings par bateau puis par date
VMS <- sortTacsat(VMS) 
### Suppression des colonnes inutiles et mise en ordre
VMS <- VMS[, c("VESSEL_FK","PAVILLON","CL_LONGUEUR","QUARTIER_IMMAT_COD",
                                     "ENGIN_FAO_COD","ENGIN_FAO_LIB","SI_DATE","SI_TIME",
                                     "LONGITUDE","LATITUDE","VITESSE","IS_FISHING","SI_DATIM")]
################ 

######## Selection des fileyeurs sur le jeu de donnees VMS ######## 
metier = read_excel("./data/step_2_real/Info_fileyeurs/TypoFiletGGanonyme_tPELAGIS.xls", sheet = "FPCtypoFiletGG")
metier <- metier %>% filter(YEARp == Annee)
### Recuperation des codes associes aux fileyeurs 
names_fileyeurs_metiers <- unique(metier$NAVS_COD) %>% as.numeric() %>% sort()
### Recuperation des codes de tous les navires VMS
names_fileyeurs_VMS <- unique(VMS$VESSEL_FK) %>% as.numeric() %>% sort()
### Selection des fileyeurs parmi tous les navires VMS
metiers_in_VMS <- which(names_fileyeurs_metiers %in% names_fileyeurs_VMS == "TRUE")
names_fileyeurs_metiers_match <- names_fileyeurs_metiers[metiers_in_VMS]
match_VMS <- which(VMS$VESSEL_FK %in% names_fileyeurs_metiers_match == "TRUE")
VMS <- VMS[match_VMS,]
rm(match_VMS,metiers_in_VMS,names_fileyeurs_metiers_match,names_fileyeurs_metiers,names_fileyeurs_VMS,metier)
################ 

######## Ecriture des fichiers formates pour future association entre AIS et VMS ######## 
file_name = paste("./data/step_2_real/AIS_VMS_to_match/",Annee,"/",Mois,".RData",sep="")
rm(Annee,Mois)
save.image(file_name)
###