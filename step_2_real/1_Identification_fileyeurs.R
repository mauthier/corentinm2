##--------------------------------------------------------------------------------------------------------
## SCRIPT : Identification des fileyeurs (co-occurence AIS et VMS)
##
## Authors : Corentin Vignard 
## Last update : 2021-08-12
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------
rm(list=ls(all=TRUE))
lapply(c("dplyr","sf","tidyverse","vmstools","doBy","sqldf","lubridate"), library, character.only = TRUE)

### Changement du nom du fichier en fonction de l'annee et du mois souhaite
Mois <- 2
Annee <- 2017
file_name = paste("./data/step_2_real/AIS_VMS_to_match/",Annee,"/",Mois,".RData",sep="")
###

### Chargement d'un RData deja formate ###
load(file_name)
rm(file_name)
biscay = st_read("./data/data_map/biscay_linestring.shp") %>% st_as_sf()
port <- read.table(file = "./data/data_map/all_port_FR.txt", sep = ";", header = TRUE) 
port <- port %>% filter(LATITUDE >= 43 & LATITUDE <= 48 & LONGITUDE >= -11 & LONGITUDE <= -0.3) %>% st_as_sf(coords = c("LONGITUDE","LATITUDE"),crs = 4326)
###

### Source des fonctions utiles
source("./functions/functions.R")
###

### Creation d'un buffer sur chaque port 
buffer_port <- port %>% 
  st_transform(crs = 2154) %>% 
  st_buffer(dist = units::set_units(3.7,km)) # 2 milles nautiques = 3.7 km
st_crs(buffer_port)$input = "WGS 84"
### Division du jeu de donnees par bateau
AIS_split <- split(AIS, AIS$VE_REF, drop = FALSE)
VMS_split <- split(VMS, VMS$VESSEL_FK, drop = FALSE)
### Suppression des navires avec seulement quelques pings
cond <- sapply(AIS_split, function(df){nrow(df) > 10})
AIS_split <- AIS_split[cond]
rm(clear = cond)
cond <- sapply(VMS_split, function(df){nrow(df) > 10})
VMS_split <- VMS_split[cond]
rm(clear = cond)
### Conversion des objets en sf data.frame ###
AIS_split <- lapply(AIS_split,df_to_sf)
VMS_split <- lapply(VMS_split,df_to_sf)
### Position relative des pings AIS par rapport au buffer_port  
AIS_split <- lapply(AIS_split,ping_in_buffer_port,buffer = buffer_port,proj_epsg = 3035)
### Suppression des pings AIS a l'arret au port 
cond <- lapply(AIS_split,function(df){df$POSITION == "TRUE" & df$VITESSE_CALCULEE <= 0.2})
for (i in 1:length(cond)){AIS_split[[i]] <- AIS_split[[i]][!cond[[i]],]}
rm(cond)
### Suppression des navires avec seulement 1 ou 2 pings 
cond <- sapply(AIS_split, function(df){nrow(df) > 2})
AIS_split <- AIS_split[cond]
rm(cond)
### Recuperation des coordonnes AIS ###
coord_AIS <- lapply(AIS_split,function(df){st_coordinates(df) %>% as.data.frame()})
### Conversion des objets sf en data.frame (necessaire pour intervalTacsat())
for (i in 1:length(AIS_split)){
  AIS_split[[i]] <- st_drop_geometry(AIS_split[[i]])
  AIS_split[[i]]$LONGITUDE <- coord_AIS[[i]]$X
  AIS_split[[i]]$LATITUDE <- coord_AIS[[i]]$Y
                              }
rm(i)
### Calcul de l'intervalle de temps (minutes) entre pings successifs ###
AIS_split <- lapply(AIS_split,function(df){
  df %>% mutate(INTV = 1)
  df$INTV <- intervalTacsat(df,level="vessel",fill.na=T)
                                          })
### Remise au format sf et identification des marees
AIS_split <- lapply(AIS_split,df_to_sf)
AIS_split <- lapply(AIS_split,tide_autre_methode)
### Restriction aux marees uniquement composees de plus de 20 pings ###
AIS_split <- lapply(AIS_split,long_tide)
### Suppression des navires avec seulement 1 ou 2 pings 
cond <- sapply(AIS_split, function(df){nrow(df) > 2})
AIS_split <- AIS_split[cond]
rm(cond)
### Position relative des pings VMS par rapport au buffer_port  
VMS_split <- lapply(VMS_split,ping_in_buffer_port,buffer = buffer_port,proj_epsg = 3035)
### Subset avec uniquement pings hors buffer_port (pour eviter les faux match)
VMS_split_out <- lapply(VMS_split,function(df){subset(df, POSITION == "FALSE")})
### Subset sur les navires ayant plus de 10 pings VMS
cond <- sapply(VMS_split_out, function(df){nrow(df) > 10})
VMS_split_out <- VMS_split_out[cond]
rm(clear = cond)
######## Buffer sur chaque maree de chaque navire AIS ######## 
buffer_maree_AIS <- lapply(AIS_split,function(df){df %>% split(df$NEW_FT_REF, drop = FALSE)})
### Creation d'un buffer polygone sur chaquee maree
buffer_maree_AIS <- lapply(buffer_maree_AIS,function(firstdf){
         firstdf <- lapply(firstdf,function(df){
    df %>% st_transform(crs = 2154) %>% 
    st_union() %>% st_cast(to = "LINESTRING") %>% 
    st_buffer(dist = units::set_units(0.5,km)) %>% 
    st_transform(crs = 4326) %>% 
    cbind(.,data.frame(start = first(df$SI_DATIM),
                         end = last(df$SI_DATIM))) %>% 
    select(start, end, geometry) %>% 
    st_as_sf()
                                               })
                                                             })
################ 

######## Creation des listes pour le calcul (en plusieurs etapes) du % de match VMS/buffer_maree ######## 
match_temporel_VMS <- pourcentage_match_VMS <- buffer_maree_AIS
list_VMS <- vector(mode = "list", length = length(VMS_split_out))
list_VMS <- lapply(VMS_split_out, function(df){names(df)}) 
for (i in 1:length(AIS_split)){
  for (j in 1:length(unique(AIS_split[[i]]$NEW_FT_REF))){
    match_temporel_VMS[[i]][[j]] <- pourcentage_match_VMS[[i]][[j]] <- list_VMS
  }
}
################ 
### Position relative des pings VMS par rapport au buffer_maree et calcul du % ###
for (i in 1:length(AIS_split)){
  for (j in 1:length(unique(AIS_split[[i]]$NEW_FT_REF))){
    for (k in 1:length(VMS_split_out)){
      match_temporel_VMS[[i]][[j]][[k]] <- VMS_split_out[[k]] %>% filter(day(SI_DATIM) >= day(buffer_maree_AIS[[i]][[j]]$start) &
                                                                                     day(SI_DATIM) <= day(buffer_maree_AIS[[i]][[j]]$end))
      pourcentage_match_VMS[[i]][[j]][[k]] <- length(which(st_within(match_temporel_VMS[[i]][[j]][[k]],buffer_maree_AIS[[i]][[j]],sparse=FALSE) == "TRUE"))*100/nrow(match_temporel_VMS[[i]][[j]][[k]])
    }
  }
}
rm(match_temporel_VMS)

######## Creation et stockage de la liste accueillant les candidats VMS potentiels ######## 
### Candidats VMS potentiels : ceux dont leur pings tombent a plus de 50 % dans le buffer_maree
candidat <- lapply(AIS_split,function(df){
  df %>% split(df$NEW_FT_REF, drop = FALSE)
})
candidat <- lapply(pourcentage_match_VMS,function(firstdf){
  firstdf <- lapply(firstdf,function(df){
    df <- names(which(df > 50))
  })
})
################ 

######## Une verification visuelle reste a faire sur les candidats (comparer pings AIS et VMS) ######## 
################ 